﻿using ImagingUtilizationUpload.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImagingUtilizationUpload.Controllers
{

    [Authorize]
    public class BaseController : Controller
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            if (User.Identity.IsAuthenticated)
            {
                if (SessionHelper.UserId == "")
                {
                    RedirectToLoginPage(filterContext);
                }
                else
                {
                    var controllerName = ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)filterContext.ActionDescriptor).ControllerName.ToString().ToLower();
                    if (controllerName.ToLower() != "home")  // && controllerName != "home"
                    {
                        if (!AuthorizationHelper.IsAuthorized(controllerName))
                        {
                            RedirectToLoginPage(filterContext);
                        }

                    }

                }
            }

        }

        private void RedirectToLoginPage(ActionExecutingContext filterContext)
        {
            var url = new UrlHelper(filterContext);
            var loginUrl = url.Action("Login", "Login");
            if (loginUrl != null)
            {
                HttpContext.Session.Clear();
                // HttpContext.SignOutAsync();
                filterContext.Result = new RedirectResult("~/Identity/Account/Login");
            }
            //filterContext.Result = new RedirectResult("~/Identity/Account/Login");
        }
    }
}
