﻿using ImagingUtilizationUpload.Data;
using ImagingUtilizationUpload.Data.Repository;
using ImagingUtilizationUpload.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ImagingUtilizationUpload.Controllers
{
    public class RoleController : BaseController
    {
        private readonly RoleManager<Roles> roleManager;
        private readonly ApplicationDbContext _context;
        private readonly IRepository<VMRoleMenuMap> _repository;
        

        // Injecting Role Manager        
        public RoleController(RoleManager<Roles> roleManager, ApplicationDbContext context, IRepository<VMRoleMenuMap> repository)
        {
            this.roleManager = roleManager;
            _context = context;
            _repository = repository;
        }
        public IActionResult Index()
        {
            return View();
        }
        //Get role list ajax
        [HttpGet]
        public JsonResult GetRoleList()
        {
            var RoleList = roleManager.Roles.Where(m => m.IsDelete == false || m.IsDelete == null).OrderByDescending(x=>x.Id).ToList();
            return new JsonResult(new { RoleList });
        }
        //Create new role
        [HttpGet]
        public async Task<IActionResult> AddRole()
        {
            return View(new Roles());
        }
        //post new role method
        [HttpPost]
        public async Task<IActionResult> AddRole(Roles role)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (role.Name == null || role.Name == "")
                    {
                        ModelState.AddModelError("Name", "Name is required");
                        return View(role);
                    }
                    role.Name.Trim();
                    var result = await roleManager.CreateAsync(role);
                    if (result.Succeeded)
                    {
                        TempData["SuccesMessage"] = "Role has been created.";
                        return RedirectToAction(nameof(Index));
                    }
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("Name", error.Description);

                    }
                }
            }
            catch(Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message;
                GenerateLogfile("Role", ex.Message);
            }
            return View(role);
        }
        //Edit role
        [HttpGet]
        public async Task<IActionResult> UpdateRole(string id)
        {
            Roles role = new Roles();
            role = await roleManager.FindByIdAsync(id);
            if (role == null)
            {
                return NotFound();
            }
            return View(role);
        }
        // post update role method
        [HttpPost]
        public async Task<IActionResult> UpdateRole(Roles roles)
        {
            try
            {
                if (roles.Name == null || roles.Name == "")
                {
                    ModelState.AddModelError("Name", "Name is required");
                    return View(roles);
                }
                var RoleId = await roleManager.FindByIdAsync(roles.Id);
                RoleId.Name = roles.Name.Trim();
                var result = await roleManager.UpdateAsync(RoleId);
                if (result.Succeeded)
                {
                    TempData["SuccesMessage"] = "Role has been updated.";
                    return RedirectToAction(nameof(Index));
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("Name", error.Description);

                }
                return View(roles);
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message;
                GenerateLogfile("Role", ex.Message);
                ModelState.AddModelError("Name", ex.Message);
                return View(roles);
            }
        }

        //Delete role
        public async Task<ActionResult> OnPostDel(string id)
        {
            var ApUser = await roleManager.FindByIdAsync(id);
            if (ApUser != null)
            {
                var RoleInUser = _context.Users.Where(m => m.RoleId == id).FirstOrDefault();
                if (RoleInUser != null)
                {
                    TempData["ErrorMessage"] = "selected Role is already assigned to user.";
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    if (ApUser.Id == id)
                    {
                        var results = _context.RoleMenuMap.Where(x => x.RoleId == ApUser.Id).ToList();
                        if (results != null && results.Count > 0)
                        {
                            _context.RoleMenuMap.RemoveRange(results);
                            _context.SaveChanges();
                        }

                        IdentityResult result = await roleManager.DeleteAsync(ApUser);

                        TempData["SuccesMessage"] = "Role has been deleted.";
                    }
                }
            }


            return RedirectToAction(nameof(Index));
        }

        public ActionResult _AssignRole(string id)
        {
            SqlParameter roleid = new SqlParameter("RoleId", (object)id ?? DBNull.Value);
            var roleMenuMaplist = _repository.ExecuteStoredProcedure<VMRoleMenuMap>("AssignRoles", roleid).OrderBy(m => m.Id).ToList();

            return PartialView("_AssignRole", roleMenuMaplist);
        }

        [HttpPost]
        public ActionResult _AssignRole(List<VMRoleMenuMap> obj)
        {
            foreach (var item in obj)
            {
                RoleMenuMap roleMenuMap = new RoleMenuMap();
                roleMenuMap.Id = item.Id;
                roleMenuMap.MenuId = item.MenuId;
                roleMenuMap.RoleId = item.RoleId;
                roleMenuMap.IsView = item.IsView;
                roleMenuMap.IsInsert = item.IsInsert;
                roleMenuMap.IsEdit = item.IsEdit;
                roleMenuMap.IsDelete = item.IsDelete;

                _context.RoleMenuMap.Update(roleMenuMap);
                _context.SaveChanges();


                //SqlParameter roleid = new SqlParameter("RoleId", (object)item.RoleId ?? DBNull.Value);
                //var Childlist = _repository.ExecuteStoredProcedure<VMRoleMenuMap>("GetRoleMenuList", roleid).Where(m => m.ParentMenuId == item.MenuId).ToList();

                //foreach (var Childitem in Childlist)
                //{
                //    RoleMenuMap roleMenuMaps = new RoleMenuMap();
                //    //if (roleMenuMap.Id == Childitem.Id) { 
                //    roleMenuMaps.Id = Childitem.Id;
                //    roleMenuMaps.MenuId = Childitem.MenuId;
                //    roleMenuMaps.RoleId = Childitem.RoleId;
                //    roleMenuMaps.IsView = item.IsView;
                //    roleMenuMaps.IsInsert = item.IsInsert;
                //    roleMenuMaps.IsEdit = item.IsEdit;
                //    roleMenuMaps.IsDelete = item.IsDelete;
                //    //}

                //    _context.RoleMenuMap.Update(roleMenuMaps);
                //    _context.SaveChanges();
                //}

            }
            TempData["SuccesMessage"] = "Rights has been assigned.";
            return RedirectToAction(nameof(Index));
        }
        public void GenerateLogfile(string ControllerName, string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\ImagingUtilizationUploadLog";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\ImagingUtilizationUploadLog\\ImagingUtilizationUploadLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_');

            if (!System.IO.File.Exists(filepath))
            {
                using (StreamWriter sw = System.IO.File.CreateText(filepath))
                {
                    sw.WriteLine("Master Page: " + ControllerName);
                    sw.WriteLine("Exception Message: " + Message);
                }
            }
            else
            {
                System.IO.File.Delete(filepath);

                using (StreamWriter sw = System.IO.File.CreateText(filepath))
                {
                    sw.WriteLine("Master Page: " + ControllerName);
                    sw.WriteLine("Exception Message: " + Message);
                }
            }
        }
    }
}
