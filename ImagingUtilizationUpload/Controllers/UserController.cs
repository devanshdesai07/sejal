﻿using ImagingUtilizationUpload.Models;
using ImagingUtilizationUpload.Common;
using ImagingUtilizationUpload.Data;
//using ImagingUtilizationUpload.Models;
using ImagingUtilizationUpload.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting.Internal;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Text.Encodings.Web;

namespace ImagingUtilizationUpload.Controllers
{
    public class UserController : Controller
    {
        private readonly UserManager<Users> _userManager;
        private readonly ApplicationDbContext _context;
        private readonly IEmailSender _emailSender;
        private IHostingEnvironment _hostingEnvironment;
        private readonly RoleManager<Roles> _roleManager;
        private IConfiguration _configuration;


        public UserController(UserManager<Users> userManager, IEmailSender emailSender,
            IHostingEnvironment environment, RoleManager<Roles> roleManager, IConfiguration iConfig, ApplicationDbContext context)
        {
            _userManager = userManager;
            _emailSender = emailSender;
            _hostingEnvironment = (IHostingEnvironment)environment;
            _roleManager = roleManager;
            _configuration = iConfig;
            _context = context;
        }
        public IActionResult Index()
        {
            ViewData["roles"] = _roleManager.Roles.Where(m => m.IsDelete == false || m.IsDelete == null).ToList();


            return View();
        }
        public JsonResult GetUserslist()
        {


            var UserList = (from F in _userManager.Users
                            join PM in _roleManager.Roles on F.RoleId equals PM.Id
                            select new
                            {
                                Id = F.Id,
                                UserName = F.UserName,
                                FirstName = F.FirstName,
                                LastName = F.LastName,
                                Email = F.Email,
                                PhoneNumber = F.PhoneNumber == null ? "" : F.PhoneNumber,
                                Name = PM.Name,
                                IsDeleted = F.IsDeleted,
                            }).Where(m => m.Id != SessionHelper.UserId).OrderByDescending(x=>x.Id).ToList();


            return new JsonResult(new { UserList });
        }
        [HttpGet]
        public ActionResult AddUsers()
        {
            ViewData["roles"] = _roleManager.Roles.Where(m => m.IsDelete == false || m.IsDelete == null).ToList();
            Users users = new Users();
            users.ImagePath = null;
            VMUsersAttachment vmobj = new VMUsersAttachment();
            vmobj.ImagePath = users.ImagePath;
            return View(vmobj);

        }
        [HttpPost]
        public async Task<IActionResult> AddUsers(VMUsersAttachment users, IFormFile file)
        {
            try
            {
                ViewData["roles"] = _roleManager.Roles.Where(m => m.IsDelete == false || m.IsDelete == null).ToList();
                if (!ModelState.IsValid)
                {
                    return View(users);
                }

                var UserAlreadyExit = _context.Users.Where(m=>m.UserName == users.UserName).FirstOrDefault();
                if (UserAlreadyExit != null)
                {
                    ModelState.AddModelError("UserName", "The user name already exists. Please use a different user name.");
                    return View(users);
                }
                var EmailAlreadyExit = _context.Users.Where(m => m.Email == users.Email).FirstOrDefault();
                if (EmailAlreadyExit != null)
                {
                    ModelState.AddModelError("Email", "The email already exists. Please use a different email.");
                    return View(users);
                }


                var supportedTypes = new[] { "jpg", "jpeg", "png", "gif" };

                if (file != null)
                {
                    var userList = await _userManager.GetUserAsync(User);

                    if (userList.Email == users.Email && userList.UserName == users.UserName)
                    {
                        Users Result = await _userManager.FindByIdAsync(userList.Id);
                        if (Result != null)
                        {
                            string Files = Path.Combine(_hostingEnvironment.WebRootPath, "ProfileImage");
                            if (System.IO.File.Exists(Files + "\\" + userList.ImagePath))
                            {
                                System.IO.File.Delete(Files + "\\" + userList.ImagePath);
                            }

                        }

                    }
                    string UploadFolder = Path.Combine(_hostingEnvironment.WebRootPath, "ProfileImage");
                    if (!Directory.Exists(UploadFolder))
                    {
                        Directory.CreateDirectory(UploadFolder);
                    }
                    var uniqueFileName = Guid.NewGuid().ToString() + "_" + file.FileName.Trim();
                    string filePath = Path.Combine(UploadFolder, uniqueFileName);
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                    users.ImagePath = uniqueFileName;
                }
                var role = _roleManager.FindByIdAsync(users.Name).Result;
                var user = new Users
                {
                    UserName = users.UserName.Trim()
                        ,
                    FirstName = users.FirstName.Trim()
                        ,
                    LastName = users.LastName.Trim()
                        ,
                    Email = users.Email.Trim()
                        ,
                    ImagePath = users.ImagePath == null ? null : users.ImagePath.Trim()
                        ,
                    PhoneNumber = users.PhoneNumber == null ? null : users.PhoneNumber
                        ,
                    EmailConfirmed = true
                        ,
                    RoleId = role.Id
                };
                if (ModelState.IsValid)
                {
                    if (users.Email == null || users.Email == "")
                    {
                        ModelState.AddModelError("Email", "Email is required");
                        return View(users);
                    }
                    if (users.UserName == null || users.UserName == "")
                    {
                        ModelState.AddModelError("UserName", "User Name is required");
                        return View(users);
                    }

                    user.EmailConfirmed = true;
                    user.IsDeleted = false;

                    var result = await _userManager.CreateAsync(user);
                    if (result.Succeeded)
                    {
                        await _userManager.AddToRoleAsync(user, role.Name);
                        var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                        var callbackUrl = Url.Page(
                            "/Account/ResetPassword",
                            pageHandler: null,
                            values: new { code },
                            protocol: Request.Scheme).Replace("/User/AddUsers", "/Identity/Account/ResetPassword");

                        string bodyTemplate = System.IO.File.ReadAllText(Path.Combine(_hostingEnvironment.WebRootPath, "EmailTemplate/ResetPasswordLink.html"));
                        bodyTemplate = bodyTemplate.Replace("[confirmationLink]", HtmlEncoder.Default.Encode(callbackUrl));

                        await _emailSender.SendEmailAsyncWithBody(user.Email, "Create your account for imaging utilization upload", bodyTemplate, true);
                        TempData["SuccesMessage"] = "User has been created.";
                        return RedirectToAction(nameof(Index));
                    }
                    foreach (var error in result.Errors)
                    {
                        if (error.Code == "DuplicateUserName")
                        {
                            ModelState.AddModelError("UserName", "UserName" + " " + user.UserName + " " + "is already taken");
                        }
                        if(error.Code == "InvalidUserName")
                        {
                            ModelState.AddModelError("UserName", error.Description);
                        }
                        else
                        {
                            ModelState.AddModelError("ErrorMessage", error.Description);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //throw ex;
                GenerateLogfile("User", ex.Message);
                TempData["ErrorMessage"] = ex.Message;

            }
            return View(users);
        }
        // get user page for update
        [HttpGet]
        public async Task<IActionResult> UpdateUsers(string id)
        {
            ViewData["roles"] = _roleManager.Roles.Where(m => m.IsDelete == false || m.IsDelete == null).ToList();
            Users Result = await _userManager.FindByIdAsync(id);
            Roles roles = new Roles();
            VMUsersAttachment users = new VMUsersAttachment();

            users.UserId = Result.Id;
            users.UserName = Result.UserName;
            users.FirstName = Result.FirstName;
            users.LastName = Result.LastName;
            users.Email = Result.Email;
            users.ImagePath = Result.ImagePath;
            users.PhoneNumber = Result.PhoneNumber == null ? null : Result.PhoneNumber;
            var roleid = await _roleManager.FindByIdAsync(Result.RoleId);
            users.Name = roleid.Id;
            users.RoleId = roleid.Id;

            if (Result == null)
            {
                return NotFound();
            }
            return View(users);
        }
        // Post update user method
        [HttpPost]
        public async Task<IActionResult> UpdateUsers(VMUsersAttachment users, IFormFile file)
        {
            try
            {
                ViewData["roles"] = _roleManager.Roles.Where(m => m.IsDelete == false || m.IsDelete == null).ToList();

                var supportedTypes = new[] { "jpg", "jpeg", "png", "gif" };

                var UserAlreadyExit = _context.Users.Where(m => m.UserName == users.UserName && m.Id != users.UserId).FirstOrDefault();
                if (UserAlreadyExit != null)
                {
                    ModelState.AddModelError("UserName", "The user name already exists. Please use a different user name.");

                    return View(users);
                }
                var EmailAlreadyExit = _context.Users.Where(m => m.Email == users.Email && m.Id != users.UserId).FirstOrDefault();
                if (EmailAlreadyExit != null)
                {
                    ModelState.AddModelError("Email", "The email already exists. Please use a different email.");
                    return View(users);
                }

                if (file != null)
                {
                    var fileExt = System.IO.Path.GetExtension(file.FileName).Substring(1);
                    if (!supportedTypes.Contains(fileExt.ToLower()))
                    {

                        ModelState.AddModelError("ImagePath", "File is invalid - Only upload .jpg/.png/.gif file");
                        return View(users);
                    }

                    Users Result = await _userManager.FindByIdAsync(users.UserId);

                    if (Result != null)
                    {
                        string Files = Path.Combine(_hostingEnvironment.WebRootPath, "ProfileImage");
                        if (System.IO.File.Exists(Files + "\\" + users.ImagePath))
                        {
                            System.IO.File.Delete(Files + "\\" + users.ImagePath);
                        }

                    }

                    string UploadFolder = Path.Combine(_hostingEnvironment.WebRootPath, "ProfileImage");
                    if (!Directory.Exists(UploadFolder))
                    {
                        Directory.CreateDirectory(UploadFolder);
                    }
                    var uniqueFileName = Guid.NewGuid().ToString() + "_" + file.FileName;
                    string filePath = Path.Combine(UploadFolder, uniqueFileName);
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                    users.ImagePath = uniqueFileName;

                }


                if (users.Email == null || users.Email == "")
                {
                    ModelState.AddModelError("Email", "Email is required");
                    return View(users);
                }
                
                var user = await _userManager.FindByIdAsync(users.UserId);
                user.UserName = users.UserName.Trim();
                user.Email = users.Email.Trim();
                user.FirstName = users.FirstName.Trim();
                user.LastName = users.LastName.Trim();
                user.PhoneNumber = users.PhoneNumber == null ? null : users.PhoneNumber.Trim();
                user.ImagePath = users.ImagePath == null ? null : users.ImagePath.Trim();
                user.EmailConfirmed = true;
                var roleid = await _roleManager.FindByIdAsync(users.Name);
                user.RoleId = roleid.Id;
                var userRole = _userManager.GetRolesAsync(user).Result.FirstOrDefault();
                if (userRole != "" && userRole != null)
                {
                    await _userManager.RemoveFromRoleAsync(user, userRole);
                }

                await _userManager.AddToRoleAsync(user, roleid.Name);

                var result = await _userManager.UpdateAsync(user);
                if (result.Succeeded)
                {
                    TempData["SuccesMessage"] = "User has been updated.";
                    return RedirectToAction(nameof(Index));
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("Email", error.Description);
                }
                return View(users);
            }
            catch (Exception ex)
            {
                //throw ex;
                GenerateLogfile("User", ex.Message);
                TempData["ErrorMessage"] = ex.Message;

            }
            return RedirectToAction(nameof(Index));
        }

        //Delete User
        public async Task<ActionResult> OnPostDel(string id)
        {
            var ApUser = await _userManager.FindByIdAsync(id);
            if (ApUser != null)
            {
                if (ApUser.Id == id)
                {

                    string Files = Path.Combine(_hostingEnvironment.WebRootPath, "ProfileImage");
                    if (System.IO.File.Exists(Files + "\\" + ApUser.ImagePath))
                    {
                        System.IO.File.Delete(Files + "\\" + ApUser.ImagePath);
                    }


                    ApUser.IsDeleted = true;
                    IdentityResult result = await _userManager.UpdateAsync(ApUser);
                }
            }
            else
            {
                ApUser.IsDeleted = false;
            }
            TempData["SuccesMessage"] = "User has been deleted.";
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public async Task<ActionResult> UpdateUserStatus(string id)
        {
            try
            {
                if (id != "" || id != null)
                {
                    var result = await _userManager.FindByIdAsync(id);
                    if (result.IsDeleted == true)
                    {
                        result.IsDeleted = false;
                    }
                    else
                    {
                        result.IsDeleted = true;
                    }

                    IdentityResult results = await _userManager.UpdateAsync(result);

                    if (results.Succeeded)
                    {
                        TempData["SuccesMessage"] = "Status has been updated.";
                        return RedirectToAction(nameof(Index));
                    }
                    foreach (var error in results.Errors)
                    {
                        ModelState.AddModelError("Email", error.Description);
                    }
                }

            }
            catch (Exception ex)
            {
                //throw ex;
                GenerateLogfile("User", ex.Message);
                TempData["ErrorMessage"] = ex.Message;
            }

            return RedirectToAction(nameof(Index));

        }

        [HttpGet]
        public async Task<JsonResult> GetProfileImage()
        {
            var user = await _userManager.GetUserAsync(User);
            return new JsonResult(new { user });
        }
        public void GenerateLogfile(string ControllerName, string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\ImagingUtilizationUploadLog";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\ImagingUtilizationUploadLog\\ImagingUtilizationUploadLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_');

            if (!System.IO.File.Exists(filepath))
            {
                using (StreamWriter sw = System.IO.File.CreateText(filepath))
                {
                    sw.WriteLine("Master Page: " + ControllerName);
                    sw.WriteLine("Exception Message: " + Message);
                }
            }
            else
            {
                System.IO.File.Delete(filepath);

                using (StreamWriter sw = System.IO.File.CreateText(filepath))
                {
                    sw.WriteLine("Master Page: " + ControllerName);
                    sw.WriteLine("Exception Message: " + Message);
                }
            }
        }
    }
}
