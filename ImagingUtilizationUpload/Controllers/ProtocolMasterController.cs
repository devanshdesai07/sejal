﻿using ImagingUtilizationUpload.Models;
using ImagingUtilizationUpload.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting.Internal;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Text.Encodings.Web;
using ImagingUtilizationUpload.Data;
using ImagingUtilizationUpload.Common;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ImagingUtilizationUpload.Controllers
{

    public class ProtocolMasterController : BaseController
    {
        private readonly ApplicationDbContext _context;

        public ProtocolMasterController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            var LanguageMaster = _context.LanguageMaster.Where(m => m.IsActive == true).OrderBy(x => x.LanguageName).ToList();
            ViewBag.LanguageMasterList = new SelectList(LanguageMaster.ToList(), "LanguageId", "LanguageName");

            return View();
        }

        [HttpGet]
        public JsonResult GetProtocolMappings(string? searchMappedProtocol = null, int? searchLanguageId = null, string? Search= null)
        {
            var protocolMappingList = (from P in _context.ProtocolMapping
                                       join LM in _context.LanguageMaster on P.LanguageId equals LM.LanguageId
                                       select new
                                       {
                                           protocolMappingId = P.ProtocolMappingId,
                                           protocolName = P.ProtocolName,
                                           mapedProtocol = P.MapedProtocol,
                                           isActive = P.IsActive,
                                           languageId = P.LanguageId,
                                           languageName = LM.LanguageName,
                                           createdBy = P.CreatedBy,
                                           createdDate = P.CreatedDate,
                                       }).Where(x => ((searchLanguageId) == 0 || searchLanguageId == null || x.languageId == (searchLanguageId))
                                          && (x.mapedProtocol.Contains(searchMappedProtocol) || string.IsNullOrEmpty(searchMappedProtocol))
                                                    && (string.IsNullOrEmpty(Search)
                                                        ||
                                                        x.languageName.Contains(Search)
                                                        ||
                                                        x.mapedProtocol.Contains(Search)
                                                        ||
                                                        x.protocolName.Contains(Search)
                                                    )
                                       ).OrderBy(m => m.languageName).OrderBy(n => n.protocolName).ToList();

            //var protocolMappingList = _context.ProtocolMapping.Where(m => m.IsActive == true)
            //                            .OrderByDescending(m => m.ProtocolMappingId).ToList();
            var Inactivecount = protocolMappingList.Where(x => x.isActive == false).Count();

            return new JsonResult(new { protocolMappingList, Inactivecount });

        
        }


        [HttpGet]
        public IActionResult ManageProtocolMaster(int Id)
        {
            ViewBag.ProtocolMasterId = Id;
            return View();
        }


        [HttpGet]
        public ActionResult ProtocolMasterDelete(int id)
        {
            try
            {
                if (id > 0)
                {
                    var result = _context.ProtocolMapping.Where(m => m.ProtocolMappingId == id).FirstOrDefault();
                    if (result.IsActive == true)
                    {
                        result.IsActive = false;
                    }
                    else
                    {
                        result.IsActive = true;
                    }
                    _context.SaveChanges();
                    TempData["SuccesMessage"] = "Protocol has been inactivated.";
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message;
                Common.HttpHelper.GenerateLogfile("ProtocolMaster", ex.Message);
            }
            return RedirectToAction(nameof(Index));
        }


        [HttpPost]
        public IActionResult ManageProtocolMaster(int Id = 0,
                string Name = "",
                string MapedProtocolSelect = "",
                string MapedLanguageSelect = "",
                string UpdateProtocolName = "",
                string UpdateMapedProtocol = "",
                int UpdateLanguages = 0
            )
        {
            try
            {
                ProtocolMapping model = new ProtocolMapping();
                model.ProtocolMappingId = Id;
                model.ProtocolName = UpdateProtocolName;
                model.MapedProtocol = UpdateMapedProtocol;
                model.IsActive = true;

                if (Id > 0)
                {
                    var result = _context.ProtocolMapping.Where(m => m.ProtocolMappingId == Id).FirstOrDefault();
                    result.ProtocolMappingId = Id;
                    result.ProtocolName = UpdateProtocolName;
                    result.MapedProtocol = UpdateMapedProtocol;
                    result.LanguageId = UpdateLanguages;
                    _context.SaveChanges();
                    TempData["SuccesMessage"] = "Protocol has been updated.";
                    return Json(true);
                }
                else
                {
                    if (Name == null || Name == "")
                    {
                        ModelState.AddModelError("Name", "Name is required");
                        return View(model);
                    }

                    string[] NameArry = { };
                    string[] MapedProtocolSelectArry = { };
                    string[] MapedLanguageSelectArry = { };


                    if (Name != "" || Name != null)
                    {
                        NameArry = Name.Split(',');
                    }
                    if (MapedProtocolSelect != "" && MapedProtocolSelect != null)
                    {
                        MapedProtocolSelectArry = MapedProtocolSelect.Split(',');
                    }
                    if (MapedLanguageSelect != "" && MapedLanguageSelect != null)
                    {
                        MapedLanguageSelectArry = MapedLanguageSelect.Split(',');
                    }

                    var i = 0;
                    for (i = 0; i < NameArry.Length; i++)
                    {
                        ProtocolMapping models = new ProtocolMapping();
                        models.ProtocolMappingId = Id;
                        models.IsActive = true;
                        models.ProtocolName = NameArry[i];
                        models.MapedProtocol = MapedProtocolSelectArry[i];
                        models.LanguageId = Int32.Parse(MapedLanguageSelectArry[i]);
                        var result = _context.ProtocolMapping.AddAsync(models);
                        _context.SaveChanges();
                    }
                    TempData["SuccesMessage"] = "Protocol master has been created.";
                    return Json(true);
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message;
                Common.HttpHelper.GenerateLogfile("ProtocolMaster", ex.Message);
                return Json(false);
            }
        }


        [HttpGet]
        public ActionResult GetProtocolMasterData(int Id)
        {
            try
            {
                if (Id > 0)
                {
                    var result = _context.ProtocolMapping.Where(m => m.ProtocolMappingId == Id).FirstOrDefault();
                    return Json(result);
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message;
                Common.HttpHelper.GenerateLogfile("ProtocolMaster", ex.Message);
            }
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public ActionResult GetMapedProtocolOptionDistinct()
        {
            try
            {
                var ProtocolMapedData = _context.ProtocolMapping
                                   .Where(m => m.IsActive == true && m.MapedProtocol != null)
                                   .ToList().OrderBy(x => x.MapedProtocol).Select(x => x.MapedProtocol).Distinct();
                return Json(ProtocolMapedData);
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message;
                Common.HttpHelper.GenerateLogfile("ProtocolMaster", ex.Message);
            }
            return RedirectToAction(nameof(Index));
        }
        [HttpGet]
        public ActionResult GetMapedProtocolOption()
        {
            try
            {
                var ProtocolMapedData = _context.ProtocolMapping
                    .Where(m => m.IsActive == true && m.MapedProtocol != null)
                    .ToList().OrderBy(x => x.MapedProtocol).Distinct();

                return Json(ProtocolMapedData);
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message;
                Common.HttpHelper.GenerateLogfile("ProtocolMaster", ex.Message);
            }
            return RedirectToAction(nameof(Index));
        }


        [HttpGet]
        public ActionResult GetLanguageOption()
        {
            try
            {
                var LanguagesData = _context.LanguageMaster
                    .Where(m => m.IsActive == true)
                    .OrderBy(x => x.LanguageName).Distinct().ToList();

                return Json(LanguagesData);
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message;
                Common.HttpHelper.GenerateLogfile("ProtocolMaster", ex.Message);
            }
            return RedirectToAction(nameof(Index));
        }


    }
}
