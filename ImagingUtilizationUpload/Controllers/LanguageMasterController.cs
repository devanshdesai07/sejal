﻿using ImagingUtilizationUpload.Models;
using ImagingUtilizationUpload.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting.Internal;
using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.IO;
using System.Text.Encodings.Web;
using ImagingUtilizationUpload.Data;
using ImagingUtilizationUpload.Common;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ImagingUtilizationUpload.Controllers
{
    public class LanguageMasterController : BaseController
    {
        private readonly ApplicationDbContext _context;
        public LanguageMasterController(ApplicationDbContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            var LanguageMaster = _context.LanguageMaster.Where(m => m.IsActive == true).OrderBy(x => x.LanguageName).ToList();
            ViewBag.LanguageMasterList = new SelectList(LanguageMaster.ToList(), "LanguageId", "LanguageName");
            return View();
        }


        [HttpGet]
        public JsonResult GetMasterLanguage(int? searchLanguageId = null)
        {
            var languageMastersList = _context.LanguageMaster.Where(x => ((searchLanguageId) == 0 || searchLanguageId == null || x.LanguageId == (searchLanguageId))
                                                                            //&&
                                                                            //(string.IsNullOrEmpty(Search) || x.LanguageName.Contains(Search))
                                                                    ).OrderBy(m => m.LanguageName).ToList();
            var Inactivecount = languageMastersList.Where(x => x.IsActive == false).Count();

           
            return new JsonResult(new { languageMastersList, Inactivecount });
        }


        [HttpGet]
        public IActionResult ManageLanguageMaster(int LanguageId)
        {
            ViewBag.LanguageMasterId = LanguageId;
            return View();
        }


        [HttpGet]
        public ActionResult LanguageMasterDelete(int id)
        {
            try
            {
                if (id > 0)
                {
                    var result = _context.LanguageMaster.Where(m => m.LanguageId == id).FirstOrDefault();
                    if (result.IsActive == true)
                    {
                        result.IsActive = false;
                    }
                    else
                    {
                        result.IsActive = true;
                    }
                    _context.SaveChanges();
                    TempData["SuccesMessage"] = "Language master has been inactivated.";
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message;
            }
            return RedirectToAction(nameof(Index));
        }


        [HttpPost]
        public async Task<IActionResult> ManageLanguageMaster(int Id = 0, string Name = "")
        {
            try
            {
                LanguageMaster model = new LanguageMaster();
                model.LanguageId = Id;
                model.LanguageName = Name;
                model.IsActive = true;

                if (Id > 0)
                {
                    var result = _context.LanguageMaster.Where(m => m.LanguageId == Id).FirstOrDefault();
                    result.LanguageId = Id;
                    result.LanguageName = Name;
                    _context.SaveChanges();
                    TempData["SuccesMessage"] = "Language master has been updated.";
                    return Json(true);
                }
                else
                {
                    if (Name == null || Name == "")
                    {
                        ModelState.AddModelError("Name", "Name is required");
                        return View(model);
                    }
                    var result = await _context.LanguageMaster.AddAsync(model);
                    _context.SaveChanges();
                    TempData["SuccesMessage"] = "Language master has been created.";
                    return Json(true);
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message;
                Common.HttpHelper.GenerateLogfile("languageMaster", ex.Message);
                return Json(false);
            }
        }

        [HttpGet]
        public ActionResult GetLanguageMasterData(int Id)
        {
            try
            {
                if (Id > 0)
                {
                    var result = _context.LanguageMaster.Where(m => m.LanguageId == Id).FirstOrDefault();
                    return Json(result);
                }
                else
                {
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message;
                Common.HttpHelper.GenerateLogfile("languageMaster", ex.Message);
            }
            return RedirectToAction(nameof(Index));
        }
    }
}
