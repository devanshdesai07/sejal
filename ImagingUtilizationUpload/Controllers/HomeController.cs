﻿using ImagingUtilizationUpload.Common;
using ImagingUtilizationUpload.Data;
using ImagingUtilizationUpload.Data.Repository;
using ImagingUtilizationUpload.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Text;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Threading;
using Microsoft.AspNetCore.Hosting;
using System.Data;
using System.Reflection;
using Dapper;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using System.Net.Http.Headers;
using Azure.Storage.Files.Shares;
using Azure;

namespace ImagingUtilizationUpload.Controllers
{
    public class HomeController : BaseController
    {
        private readonly ApplicationDbContext _context;
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment webHostEnvironment;
      
        public HomeController(ApplicationDbContext context, IConfiguration configuration, IWebHostEnvironment webHostEnvironment)
        {
            _context = context;
            _configuration = configuration;
            this.webHostEnvironment = webHostEnvironment;
        }

        public IActionResult Index()
        {
           
            return View();
        }

        public IActionResult GetTokenDetails()
        {
            try
            {
                SalesforceClient salesfclient = new SalesforceClient(_configuration);
                var tokendetails = salesfclient.generateSalesForceToken();
                var objToken = JsonConvert.DeserializeObject<TokenDetails>(tokendetails);
                if (objToken != null)
                {
                    TempData["Token"] = objToken.AccessToken;
                    Authorization objauth = new Authorization
                    {
                        AccessToken = objToken.AccessToken,
                        InstanceUrl = objToken.InstanceUrl,
                        IssuedAt = objToken.IssuedAt,
                        Signature = objToken.Signature,
                        TokenType = objToken.TokenType
                    };
                    _context.Authorization.RemoveRange();
                    _context.Authorization.Add(objauth);
                }
                return new JsonResult(new { tokendetails, success = true });
            }
            catch (Exception ex)
            {
                return new JsonResult(new { errormsg = ex.Message, success = false });
            }
        }

        public IActionResult GetSerialNumberAccountdetails(string token, string Serialnumber)
        {
            try
            {
                SalesforceClient salesfclient = new SalesforceClient(_configuration);
                var salesfAccount = salesfclient.getAccountDetails(token, Serialnumber);
                var objSalesfAccount = JsonConvert.DeserializeObject<SalesForceAccount>(salesfAccount);
                return new JsonResult(new { objSalesfAccount, success = true });
            }
            catch (Exception ex)
            {
                return new JsonResult(new { errormsg = ex.Message, success = false });
            }
        }

        public IActionResult GetAddressDetails(string token, string Account__c)
        {
            try
            {
                SalesforceClient salesfclient = new SalesforceClient(_configuration);
                var salesfAccount = salesfclient.GetAddressDetails(token, Account__c);
                var objSalesfAccount = JsonConvert.DeserializeObject<SalesForceAccount>(salesfAccount);
                return new JsonResult(new { objSalesfAccount, success = true });
            }
            catch (Exception ex)
            {

                return new JsonResult(new { errormsg = ex.Message, success = false });
            }
        }

        public IActionResult GetDeviceName(string deviceName)
        {
            try
            {
                bool deviceNameIsFound = false;
                var dName = _context.DeviceType.Where(x => x.DeviceName == deviceName).FirstOrDefault();
                if (dName != null)
                {
                    deviceNameIsFound = true;
                }
                else
                {
                    deviceNameIsFound = false;

                }
                return new JsonResult(new { deviceNameIsFound, success = true });
            }
            catch (Exception ex)
            {
                return new JsonResult(new { success = false, errormsg = ex.Message });
            }
        }
        public string ReadDB(string SerialNumber, string SalesForceDetails, string DBFilePath, string GetSerialNumberAccountdetails, string GetAddressDetails, int LanguageId,string filePathLocal)
        {
            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection(filePathLocal);
            string str = ReadData(sqlite_conn, SerialNumber, SalesForceDetails, DBFilePath, GetSerialNumberAccountdetails, GetAddressDetails, LanguageId);

            return str;
        }

        static SQLiteConnection CreateConnection(string DBFilePath)
        {

            SQLiteConnection sqlite_conn;
            //SQLiteConnection sqlite_conn1;
            // Create a new database connection:
            //sqlite_conn = new SQLiteConnection("Data Source=database.db; Version = 3; New = True; Compress = True; ");
            //sqlite_conn = new SQLiteConnection(@"URI=file:C:\Projects\sejal\ImagingUtilizationUpload\wwwroot\UploadedDB\SampleDBFile.db");
            //sqlite_conn1 = new SQLiteConnection(@"Data Source=https://imagingutilizationfile.file.core.windows.net/imagingfileshare/imagingfileshare/Sample DB File.db;Version=3;New=True;Compress=True;", true);
            sqlite_conn = new SQLiteConnection(@"URI=file:" + DBFilePath);

            // Open the connection:
            try
            {
                sqlite_conn.Open();
            }
            catch (Exception ex)
            {

            }
            return sqlite_conn;
        }

        public string ReadData(SQLiteConnection conn, string SerialNumber, string SalesForceDetails, string DBFilePath, string GetSerialNumberAccountdetails, string GetAddressDetails, int LanguageId)
        {
            string result = "";
            SqlConnection conns = null;
            try
            {
                string TablesList = _configuration.GetSection("Tables").GetSection("TablesList").Value;
                string query = "";
                string[] values = TablesList.Split(',');
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = values[i].Trim();
                }

                foreach (string s in values)
                {
                    query += "SELECT * FROM " + s.ToString() + ";";
                }

                SQLiteDataAdapter myAdapter = new SQLiteDataAdapter(query, conn);
                myAdapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;
                DataSet myDataSet = new DataSet();
                myAdapter.Fill(myDataSet);

                //string json_Version = JsonConvert.SerializeObject(myDataSet.Tables[0], Formatting.Indented);
                string json_ContextUserApp = JsonConvert.SerializeObject(myDataSet.Tables[1], Formatting.Indented);
                string json_ContextExam = JsonConvert.SerializeObject(myDataSet.Tables[2], Formatting.Indented);
                string json_ContextExamProcedure = JsonConvert.SerializeObject(myDataSet.Tables[3], Formatting.Indented);
                string json_Acquisition = JsonConvert.SerializeObject(myDataSet.Tables[4], Formatting.Indented);
                //string json_AcquisitionEvent = JsonConvert.SerializeObject(myDataSet.Tables[5], Formatting.Indented);
                string json_Error = JsonConvert.SerializeObject(myDataSet.Tables[6], Formatting.Indented);
                //string json_Image = JsonConvert.SerializeObject(myDataSet.Tables[7], Formatting.Indented);
                //string json_EosSystemInfos = JsonConvert.SerializeObject(myDataSet.Tables[8], Formatting.Indented);

                conn.Close();


                //sp_Insert_MasterSP

                string connectionstr = _configuration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;
                conns = new SqlConnection(connectionstr);
                SqlCommand cmd = new SqlCommand("sp_Insert_MasterSqlite", conns);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 32767;
                //cmd.Parameters.AddWithValue("@json1", json_Version);
                cmd.Parameters.AddWithValue("@json2", json_ContextUserApp);
                cmd.Parameters.AddWithValue("@json3", json_ContextExam);
                cmd.Parameters.AddWithValue("@json4", json_ContextExamProcedure);
                cmd.Parameters.AddWithValue("@json5", json_Acquisition);
                //cmd.Parameters.AddWithValue("@json6", json_AcquisitionEvent);
                cmd.Parameters.AddWithValue("@json7", json_Error);
                //cmd.Parameters.AddWithValue("@json8", json_Image);
                //cmd.Parameters.AddWithValue("@json9", json_EosSystemInfos);
                cmd.Parameters.AddWithValue("@SerialNumber", SerialNumber.ToString());
                cmd.Parameters.AddWithValue("@SalesForceDetails", SalesForceDetails.ToString());
                cmd.Parameters.AddWithValue("@GetSerialNumberAccountdetails", GetSerialNumberAccountdetails.ToString());
                cmd.Parameters.AddWithValue("@GetAddressDetails", GetAddressDetails.ToString());
                cmd.Parameters.AddWithValue("@DBFilePath", DBFilePath.ToString());
                cmd.Parameters.AddWithValue("@LanguageId", LanguageId);
                conns.Open();
                //if (cmd.ExecuteScalar() != null && cmd.ExecuteScalar() != "")
                //{
                result = Convert.ToString(cmd.ExecuteScalar());
               
                //}
                //else
                //{
                //result = "0";
                //}             
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conns.Close();
            }
        }


        [HttpPost]

        [DisableRequestSizeLimit, RequestFormLimits(MultipartBodyLengthLimit = int.MaxValue, ValueLengthLimit = int.MaxValue)]
        public IActionResult OnPostMyUploader(IFormFile MyUploader, string SerialNumber, string SalesForceDetails, string GetSerialNumberAccountdetails, string GetAddressDetails, int LanguageId)
        {
            try
            {
                if (MyUploader != null)
                {
                    string uploadsFolder = Path.Combine(webHostEnvironment.WebRootPath, "UploadedDB");                    
                    System.IO.DirectoryInfo di = new DirectoryInfo(uploadsFolder);
                    try
                    {
                        foreach (FileInfo file in di.GetFiles())
                        {
                            file.Delete();
                        }
                    }
                    catch { }
                    string filePathLocal = Path.Combine(uploadsFolder, DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" + MyUploader.FileName);
                    using (var fileStream = new FileStream(filePathLocal, FileMode.Create))
                    {
                        MyUploader.CopyTo(fileStream);
                    }
                    string mimeType = MyUploader.ContentType;
                    byte[] fileData = new byte[MyUploader.Length];
                    string FileName = MyUploader.FileName;
                    string filePath = UploadFileToBlob(FileName, fileData, mimeType, MyUploader);
                    if (filePath != "")
                    {
                        string str = ReadDB(SerialNumber, SalesForceDetails, filePath, GetSerialNumberAccountdetails, GetAddressDetails, LanguageId, filePathLocal);
                        return new ObjectResult(new { status = "success", MasterSqlLiteId = Convert.ToInt32(str) });
                    }
                    else
                    {
                        return new ObjectResult(new { status = "fail", MessageText = "File Not saved" });
                    }                                        
                }
                else
                {
                    return new ObjectResult(new { status = "fail", MessageText = "Data not getting" });
                }
            }
            catch (Exception ex)
            {
                return new ObjectResult(new { status = "fail", MessageText = ex.Message.ToString() });
            }
        }

        private string GenerateFileName(string fileName)
        {
            string strFileName = string.Empty;
            string[] strName = fileName.Split('.');
            strFileName = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd") + "/" + DateTime.Now.ToUniversalTime().ToString("yyyyMMdd\\THHmmssfff") + "." + strName[strName.Length - 1];
            return strFileName;
        }
        private string UploadFileToBlob(string strFileName, byte[] fileData, string fileMimeType, IFormFile MyUploader)
        {
            try
            {               
                string AzurConn = _configuration.GetSection("ConnectionStrings").GetSection("AccessKey").Value;
                BlobServiceClient blobServiceClient = new BlobServiceClient(AzurConn);
                string strContainerName = _configuration.GetSection("AzureDetails").GetSection("ContainerName").Value;
                BlobContainerClient container = blobServiceClient.GetBlobContainerClient(strContainerName);


                container.CreateIfNotExists(PublicAccessType.BlobContainer);

                string fileName = ContentDispositionHeaderValue.Parse(MyUploader.ContentDisposition).FileName.Trim('"');
                var uniqueFileName = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd") + "_" + DateTime.Now.ToUniversalTime().ToString("HH_mm_ss_tt") + "_" + Convert.ToString(fileName);

                var newBlob = container.GetBlobClient(uniqueFileName);
                newBlob.Upload(MyUploader.OpenReadStream());
                return Convert.ToString(newBlob.Uri.OriginalString);

                /*CODE FOR Store File in using File share
                ShareClient share = new(AzurConn, "imagingfileshare");

                var directory = share.GetDirectoryClient("imagingfileshare");

                var file = directory.GetFileClient(uniqueFileName);
              
                file.Create(MyUploader.Length);
               var ss= file.UploadRange(
                    new HttpRange(0, MyUploader.Length),
                    MyUploader.OpenReadStream());
                return "";*/
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        public IActionResult GetLanguageList()
        {
            var LanguageMaster = _context.LanguageMaster.Where(m => m.IsActive == true).OrderBy(x => x.LanguageName).ToList();
            return new JsonResult(new { LanguageMaster });
        }

        public IActionResult GetFetchUnMatchedProtocals(int MasterSqliteId, int LanuguageId)
        {

            SqlConnection conns = null;
            DataTable dtUnMatchedProtocol = new DataTable();

            string connectionstrs = _configuration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;
            conns = new SqlConnection(connectionstrs);
            SqlCommand cmds = new SqlCommand("FetchUnMatchedProtocals", conns);
            cmds.CommandType = CommandType.StoredProcedure;
            cmds.CommandTimeout = 32767;
            cmds.Parameters.AddWithValue("@MasterSqliteId", MasterSqliteId);
            cmds.Parameters.AddWithValue("@LanguageId", LanuguageId);
            SqlDataAdapter sda = new SqlDataAdapter(cmds);
            sda.Fill(dtUnMatchedProtocol);
            List<string> lst = ConvertDataTable<string>(dtUnMatchedProtocol);
            return new ObjectResult(new { status = "success", unMatchedProtocol = lst });
        }

        [HttpGet]
        public ActionResult GetMapedProtocolOption()
        {
            try
            {
                var ProtocolMapedData = _context.ProtocolMapping
                    .Where(m => m.IsActive == true && m.MapedProtocol != null)
                    .ToList().OrderBy(x => x.MapedProtocol).Select(x => x.MapedProtocol).Distinct();

                return Json(ProtocolMapedData);
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message;
                Common.HttpHelper.GenerateLogfile("ProtocolMaster", ex.Message);
            }
            return RedirectToAction(nameof(Index));
        }

        public List<string> ConvertDataTable<T>(DataTable dt)
        {
            List<string> data = new List<string>();
            foreach (DataRow row in dt.Rows)
            {
                data.Add(row[0].ToString());
            }
            return data;
        }


        public IActionResult UpdateSqlLiteMappedData(int MasterSqliteId, int languageId)
        {
            try
            {
                SqlConnection conns = null;
                DataTable dtUnMatchedProtocol = new DataTable();
                string connectionstrs = _configuration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;
                conns = new SqlConnection(connectionstrs);
                SqlCommand cmds = new SqlCommand("UpdateSqlLiteMappedData", conns);
                cmds.CommandType = CommandType.StoredProcedure;
                cmds.CommandTimeout = 32767;
                cmds.Parameters.AddWithValue("@MasterSqliteId", MasterSqliteId);
                cmds.Parameters.AddWithValue("@languageId", languageId);
                SqlDataAdapter sda = new SqlDataAdapter(cmds);
                sda.Fill(dtUnMatchedProtocol);
                List<string> lst = ConvertDataTable<string>(dtUnMatchedProtocol);
                return new ObjectResult(new { status = true, unMatchedProtocol = lst });
            }
            catch (Exception ex)
            {
                return new ObjectResult(new { status = false, errormsg = ex.Message });
            }
        }


        public async Task<IActionResult> UploadDataToSalesforce(int MasterSqliteId, string Device__c, string Name, string token)
        {
            string results = "";
            string messagetext = "";
            int TotalRecords = 0;
            bool hasErrors = true;
            SalesForceAccount objSalesfAccount = new SalesForceAccount();
            SqlConnection conns = null;
            string connectionstrs = _configuration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;
            conns = new SqlConnection(connectionstrs);


            var parameters = new DynamicParameters();

            parameters.Add("@MasterSqliteId", MasterSqliteId, DbType.Int32, ParameterDirection.Input, MasterSqliteId);
            parameters.Add("@Device__c", Device__c, DbType.String, ParameterDirection.Input, Device__c.Length);
            parameters.Add("@Name", Name, DbType.String, ParameterDirection.Input, Name.Length);

            var result = conns.QueryMultiple("GetDataForSalesforceAPI", parameters, commandType: CommandType.StoredProcedure);
            var res = result.Read<string>();

            try
            {

                var fullResult = string.Concat(res);
                Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(Convert.ToString(fullResult));
                int Start = 0;
                int End = 0;
                int Take = 200;

                if (myDeserializedClass != null && myDeserializedClass.records.Count > 0)
                {
                    TotalRecords = myDeserializedClass.records.Count;

                    while (End <= TotalRecords)
                    {
                        End = End + 200;
                        if (End > TotalRecords)
                        {
                            Take = TotalRecords - (End - 200) - 1;
                            End = TotalRecords + 1;
                        }
                        else
                        {
                            Take = 200;
                        }

                        var desiredJson = "{\"records\" :" + Convert.ToString(JsonConvert.SerializeObject(myDeserializedClass.records.GetRange(Start, Take))) + "}";// ;

                        //fetch records from the list where start to end and CALL the sales force API

                        SalesforceClient salesfclient = new SalesforceClient(_configuration);
                        var UploadDataToSalesforce = await salesfclient.UploadDataToSalesforce(token, desiredJson);
                        try
                        {
                            var UploadDataToSalesforcedata = JsonConvert.DeserializeObject<Roots>(UploadDataToSalesforce);

                            if (UploadDataToSalesforcedata.hasErrors == false)
                            {
                                hasErrors = false;
                            }
                            else
                            {
                                var parameters1 = new DynamicParameters();
                                parameters1.Add("@MasterSqliteId", MasterSqliteId, DbType.Int32, ParameterDirection.Input, MasterSqliteId);
                                parameters1.Add("@Errortxt", UploadDataToSalesforcedata, DbType.String, ParameterDirection.Input);

                                var resultss = conns.QueryMultiple("sp_Add_ErrorLogTxt", parameters1, commandType: CommandType.StoredProcedure);
                            }
                        }
                        catch (Exception ex)
                        {
                            var parameters1 = new DynamicParameters();
                            parameters1.Add("@MasterSqliteId", MasterSqliteId, DbType.Int32, ParameterDirection.Input, MasterSqliteId);
                            parameters1.Add("@Errortxt", UploadDataToSalesforce, DbType.String, ParameterDirection.Input);

                            var resultss = conns.QueryMultiple("sp_Add_ErrorLogTxt", parameters1, commandType: CommandType.StoredProcedure);
                        }
                        //if (objSalesfAccount.done == true)
                        //{
                        //    var parameters1 = new DynamicParameters();

                        //    parameters1.Add("@MasterSqliteId", MasterSqliteId, DbType.Int32, ParameterDirection.Input, MasterSqliteId);
                        //    parameters1.Add("@SalesforceId", Device__c, DbType.String, ParameterDirection.Input, objSalesfAccount.id);

                        //    var resultss = conns.QueryMultiple("sp_Insert_SalesforceId", parameters, commandType: CommandType.StoredProcedure);
                        //    var ress = result.Read<string>();
                        //}

                        Start = End + 1;
                    }
                    if (hasErrors == false)
                    {
                        var parameters1 = new DynamicParameters();
                        parameters1.Add("@MasterSqliteId", MasterSqliteId, DbType.Int32, ParameterDirection.Input, MasterSqliteId);
                        parameters1.Add("@NoOfProcessedRecord", TotalRecords, DbType.Int32, ParameterDirection.Input, TotalRecords);

                        var resultss = conns.QueryMultiple("sp_Insert_NoOfProcessedRecord", parameters1, commandType: CommandType.StoredProcedure);
                    }
                }
                else
                {
                    hasErrors = true;
                    messagetext = "No Record found for process.";
                }
            }
            catch (Exception ex)
            {
                hasErrors = true;
                messagetext = ex.Message.ToString();
            }


            return new JsonResult(new { hasErrors = hasErrors, messagetext = messagetext, TotalRecords = TotalRecords });
        }

        public IActionResult GetLastUtilizationDate(string SerialNumber)
        {
            try
            {
                SqlConnection conns = null;
                DataTable dtUnMatchedProtocol = new DataTable();
                string result = "";

                string connectionstrs = _configuration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;
                conns = new SqlConnection(connectionstrs);
                SqlCommand cmds = new SqlCommand("select Top 1 CreatedDate from MasterSqlite where SerialNumber ='" + SerialNumber + "' order by Id desc ", conns);
                conns.Open();
                if (cmds.ExecuteScalar() != null && cmds.ExecuteScalar() != "")
                {
                    result = cmds.ExecuteScalar().ToString();
                }
                else
                {
                    result = "-";
                }
                return new JsonResult(new { result });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public async Task<IActionResult> NoOfProcessedReocrds(int MasterSqliteId, string Device__c, string Name, string token)
        //{
        //    int TotalRecords = 0;
        //    SqlConnection conns = null;
        //    string connectionstrs = _configuration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;
        //    conns = new SqlConnection(connectionstrs);


        //    var parameters = new DynamicParameters();

        //    parameters.Add("@MasterSqliteId", MasterSqliteId, DbType.Int32, ParameterDirection.Input, MasterSqliteId);
        //    parameters.Add("@Device__c", Device__c, DbType.String, ParameterDirection.Input, Device__c.Length);
        //    parameters.Add("@Name", Name, DbType.String, ParameterDirection.Input, Name.Length);

        //    var result = conns.QueryMultiple("GetDataForSalesforceAPI", parameters, commandType: CommandType.StoredProcedure);
        //    var res = result.Read<string>();

        //    try
        //    {

        //        var fullResult = string.Concat(res);

        //        Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(Convert.ToString(fullResult));

        //        if (myDeserializedClass != null && myDeserializedClass.records.Count > 0)
        //        {
        //            TotalRecords = myDeserializedClass.records.Count;

        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    return new JsonResult(TotalRecords);
        //}


        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
        public class Attributes
        {
            public string referenceId { get; set; }
            public string type { get; set; }
        }

        public class Record
        {
            public Attributes attributes { get; set; }
            public string Procedure_Id__c { get; set; }
            public string Start_Time__c { get; set; }
            public string Morphotype__c { get; set; }
            public string Protocol_Name__c { get; set; }
            public string Acquisition_Type__c { get; set; }
            public string Acquisition_Lanes__c { get; set; }
            public string RRA_Status__c { get; set; }
            public string SessionName__c { get; set; }
            public string Devices_and_Hardware__c { get; set; }
            public string Name { get; set; }
            public int PatientAge__c { get; set; }
        }

        public class Root
        {
            public List<Record> records { get; set; }
        }


        public class Error
        {
            public string statusCode { get; set; }
            public string message { get; set; }
            public List<string> fields { get; set; }
        }

        public class Result
        {
            public string referenceId { get; set; }
            public List<Error> errors { get; set; }
        }

        public class Roots
        {
            public bool hasErrors { get; set; }
            public List<Result> results { get; set; }
        }


    }
}

