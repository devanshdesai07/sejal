﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Hosting;
using ImagingUtilizationUpload.Data.Repository;
using ImagingUtilizationUpload.Data;
using Microsoft.Extensions.Hosting.Internal;
using Microsoft.Extensions.Options;
using ImagingUtilizationUpload.Common;
using ImagingUtilizationUpload.Models;
using System.IO;
using ImagingUtilizationUpload.Services;
using Microsoft.Data.SqlClient;

namespace ImagingUtilizationUpload.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class LoginModel : PageModel
    {
        private readonly SignInManager<Users> _signInManager;
        private readonly UserManager<Users> _userManager;
        private readonly ILogger<LoginModel> _logger;
        private readonly IEmailSender _emailSender;
        private IHostingEnvironment _hostingEnvironment;
        private readonly ConfigSetting _appConfigSetting;
        private readonly RoleManager<Roles> _roleManager;
        private readonly IRepository<RoleMenuList> _repository;
        private readonly ApplicationDbContext _context;

        public LoginModel(SignInManager<Users> signInManager, ILogger<LoginModel> logger, UserManager<Users> userManager
            , RoleManager<Roles> roleManager, IHostingEnvironment environment, IEmailSender emailSender, IOptions<ConfigSetting> appConfigSetting
            ,IRepository<RoleMenuList> repository
             , ApplicationDbContext context)
        {
            _signInManager = signInManager;
            _logger = logger;
            _userManager = userManager;
            _emailSender = emailSender;
            _hostingEnvironment = (IHostingEnvironment)environment;
            _appConfigSetting = appConfigSetting.Value;
            _roleManager = roleManager;
            _repository = repository;
            _context = context;

        }
        [BindProperty]
        public InputModel Input { get; set; }

        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        public string ReturnUrl { get; set; }

        [TempData]
        public string ErrorMessage { get; set; }

        [TempData]
        public string Message { get; set; }

        public class InputModel
        {
            [Required]
            [EmailAddress]
            public string Email { get; set; }

            public string Id { get; set; }

            [Required]
            [DataType(DataType.Password)]
            public string Password { get; set; }

            [Display(Name = "Remember me?")]
            public bool RememberMe { get; set; }
        }

        public async Task OnGetAsync(string returnUrl = null)
        {
            if (!string.IsNullOrEmpty(ErrorMessage))
            {
                ModelState.AddModelError(string.Empty, ErrorMessage);
            }

            returnUrl = returnUrl ?? Url.Content("~/");

            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();

            ReturnUrl = returnUrl;
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            ViewData["IsConfirmed"] = false;
            ViewData["IsPasswordExpired"] = false;
            ViewData["IsSetPassword"] = false;
            returnUrl = returnUrl ?? Url.Content("~/");

            Users users = new Users();

            // var results = await _userManager.FindByNameAsync(Input.UserName);
            var results = _context.Users.Where(m => m.UserName == Input.Email || m.Email == Input.Email).FirstOrDefault();

            if (results == null)
            {
                ModelState.AddModelError("Input.Email", "Please enter valid Email Address ");
                return Page();
            }
            if (results.PasswordHash == null)
            {
                ViewData["IsSetPassword"] = true;
                ModelState.AddModelError("Input.Email", "your password is not set, please check your mail for set password");
                return Page();
            }
            if (ModelState.IsValid)
            {

                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true


                //var user = await _userManager.FindByNameAsync(Input.UserName);
                var user = _context.Users.Where(m => m.Email == Input.Email).FirstOrDefault();

                if (user != null)
                {
                    if (user.IsDeleted == true)
                    {
                        ModelState.AddModelError("Input.Email", "Your account has been deactivated. Please contact administrator");
                        return Page();
                    }
                    if (!await _userManager.IsEmailConfirmedAsync(user))
                    {
                        ViewData["IsConfirmed"] = true;
                        ModelState.AddModelError("Input.Email", "You must have a confirmed email to log in.");
                        return Page();
                    }
                    if (user.PasswordHash == users.PasswordHash)
                    {
                        ViewData["IsPasswordExpired"] = true;
                        ModelState.AddModelError("Input.Email", "your password is not set, please check your mail for set password");
                        return Page();

                    }
                    if (user.PasswordExpiryDate.Value.AddDays(_appConfigSetting.passwordExpiryDay) < DateTime.UtcNow)
                    {
                        ViewData["IsPasswordExpired"] = true;
                        ModelState.AddModelError("Input.Email", "Your password has been expired.");
                        return Page();
                    }

                }
              
                var result = await _signInManager.PasswordSignInAsync(user, Input.Password, Input.RememberMe, lockoutOnFailure: true);
                if (result.Succeeded)
                {
                    try
                    {
                        SessionHelper.UserId = Convert.ToString(results.Id);

                        List<RoleMenuList> list = new List<RoleMenuList>();
                        SqlParameter roleName = new SqlParameter("RoleId", (object)results.RoleId ?? DBNull.Value);
                        list = _repository.ExecuteStoredProcedure<RoleMenuList>("GetRoleMenuList", roleName).ToList();
                        SessionHelper.RoleMenuList = list;

                        _logger.LogInformation("User logged in.");
                        TempData["SuccesMessage"] = "Logged in successfully.";
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                   // return Page();
                    return LocalRedirect(returnUrl);
                   // return RedirectToAction("Index", "Home");
                   // return Redirect("/Home/Index");
                }

                else
                {
                    ModelState.AddModelError("Input.Password", "Invalid login credentials.");
                    return Page();
                }
            }
           // return RedirectToAction("Index", "Home");
            // If we got this far, something failed, redisplay form
            return Page();

        }

        // send verification link
        public async Task<IActionResult> OnGetSendVerificationLinkAsync(string username = null)
        {

            bool Issuccess = false;
            //var userData = await _userManager.FindByNameAsync(username);
            //  var userData = await _userManager.FindByEmailAsync(email);
            var userData = _context.Users.Where(x => x.Email == username).FirstOrDefault();
            if (userData != null)
            {

                var code = await _userManager.GenerateEmailConfirmationTokenAsync(userData);
                var callbackUrl = Url.Page(
                    "/Account/ConfirmEmail",
                    pageHandler: null,
                    values: new { userId = userData.Id, code = code },
                    protocol: Request.Scheme);

                string bodyTemplate = System.IO.File.ReadAllText(Path.Combine(_hostingEnvironment.WebRootPath, "EmailTemplate/EmailVerificationLink.html"));
                bodyTemplate = bodyTemplate.Replace("[confirmationLink]", HtmlEncoder.Default.Encode(callbackUrl));

                await _emailSender.SendEmailAsyncWithBody(userData.Email, "Email Verification", bodyTemplate, true);
                Issuccess = true;
            }
            else
            {
                Issuccess = false;
            }
            return new JsonResult(new { success = Issuccess });
        }
        // send reset password link
        public async Task<IActionResult> OnGetSendResetPasswordLinkAsync(string username = null)
        {

            bool Issuccess = false;
            // var userData = await _userManager.FindByNameAsync(username);
            var userData = _context.Users.Where(x => x.Email == username).FirstOrDefault();
            if (userData != null)
            {
                var code = await _userManager.GeneratePasswordResetTokenAsync(userData);
                var callbackUrl = Url.Page(
                    "/Account/ResetPassword",
                    pageHandler: null,
                    values: new { code },
                    protocol: Request.Scheme);

                string bodyTemplate = System.IO.File.ReadAllText(Path.Combine(_hostingEnvironment.WebRootPath, "EmailTemplate/ForgotPasswordLink.html"));
                bodyTemplate = bodyTemplate.Replace("[confirmationLink]", HtmlEncoder.Default.Encode(callbackUrl));

                await _emailSender.SendEmailAsyncWithBody(userData.Email, "Forgot Password", bodyTemplate, true);

                Issuccess = true;
            }
            else
            {
                Issuccess = false;
            }
            return new JsonResult(new { success = Issuccess });
        }
        // send set password link
        public async Task<IActionResult> OnGetSendSetPasswordLinkAsync(string username = null)
        {

            bool Issuccess = false;
            var userData = _context.Users.Where(x => x.Email == username).FirstOrDefault();
            if (userData != null)
            {
                var code = await _userManager.GeneratePasswordResetTokenAsync(userData);
                var callbackUrl = Url.Page(
                    "/Account/ResetPassword",
                    pageHandler: null,
                    values: new { code },
                    protocol: Request.Scheme);

                string bodyTemplate = System.IO.File.ReadAllText(Path.Combine(_hostingEnvironment.WebRootPath, "EmailTemplate/ResetPasswordLink.html"));
                bodyTemplate = bodyTemplate.Replace("[confirmationLink]", HtmlEncoder.Default.Encode(callbackUrl));

                await _emailSender.SendEmailAsyncWithBody(userData.Email, "Create your account for Imaging Utilization Upload", bodyTemplate, true);


                Issuccess = true;
            }
            else
            {
                Issuccess = false;
            }
            return new JsonResult(new { success = Issuccess });
        }

    }
}
