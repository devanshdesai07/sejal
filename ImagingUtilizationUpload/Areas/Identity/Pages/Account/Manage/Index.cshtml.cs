﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using ImagingUtilizationUpload.Areas.Identity.Pages.Account;
using ImagingUtilizationUpload.Models;
using ImagingUtilizationUpload.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
//using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Hosting.Internal;
using Microsoft.Extensions.Logging;

namespace ImagingUtilizationUpload.Areas.Identity.Pages.Account.Manage
{
    public partial class IndexModel : PageModel
    {
        private readonly UserManager<Users> _userManager;
        private readonly SignInManager<Users> _signInManager;
        //private readonly ILogger<LogoutModel> _logger;
        private readonly IEmailSender _emailSender;
        private IHostingEnvironment _hostingEnvironment;

        public IndexModel(
           UserManager<Users> userManager,
           SignInManager<Users> signInManager,
           IEmailSender emailSender,
            //  ILogger<LogoutModel> logger,
            IHostingEnvironment environment)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _hostingEnvironment = (IHostingEnvironment)environment;
            _emailSender = emailSender;
          //  _logger = logger;
        }
        public bool IsEmailConfirmed { get; set; }
        public string Username { get; set; }

        [TempData]
        public string StatusMessage { get; set; }

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            [Required]
            [EmailAddress]
            public string Email { get; set; }

            [Required]
            [Display(Name = "First Name")]
            public string FirstName { get; set; }

            [Display(Name = "User Name")]
            public string UserName { get; set; }

            [Required]
            [Display(Name = "Last Name")]
            public string LastName { get; set; }

            public string ImagePath { get; set; }

            // [Phone]
            [Required]
            [RegularExpression(@"^\(\d{3}\)\s{0,1}\d{3}-\d{4}$", ErrorMessage = "Enter a valid Phone number")]
            [Display(Name = "Phone Number")]
            public string PhoneNumber { get; set; }


        }

        //private async Task LoadAsync(Users user)
        //{
        //    var userName = await _userManager.GetUserNameAsync(user);
        //    var phoneNumber = await _userManager.GetPhoneNumberAsync(user);

        //    Username = userName;

        //    Input = new InputModel
        //    {
        //        PhoneNumber = phoneNumber
        //    };
        //}

        public async Task<IActionResult> OnGetAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                TempData["ErrorMessage"] = "Unable to load user with ID";
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var userName = await _userManager.GetUserNameAsync(user);
            var email = await _userManager.GetEmailAsync(user);
            var phoneNumber = await _userManager.GetPhoneNumberAsync(user);



            Input = new InputModel
            {
                UserName = userName,
                Email = email,
                PhoneNumber = phoneNumber,
                FirstName = user.FirstName,
                LastName = user.LastName,
                ImagePath = user.ImagePath,
             
            };

            IsEmailConfirmed = await _userManager.IsEmailConfirmedAsync(user);

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(IFormFile file)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                TempData["ErrorMessage"] = "Unable to load user with ID";
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var email = await _userManager.GetEmailAsync(user);
            if (Input.Email != email)
            {
                var setEmailResult = await _userManager.SetEmailAsync(user, Input.Email);
                if (!setEmailResult.Succeeded)
                {
                    var userId = await _userManager.GetUserIdAsync(user);
                    throw new InvalidOperationException($"Unexpected error occurred setting email for user with ID '{userId}'.");
                }
                await _signInManager.SignOutAsync();
              //  _logger.LogInformation("User logged out.");
                return RedirectToPage("./Login");
            }

            var phoneNumber = await _userManager.GetPhoneNumberAsync(user);
            if (Input.PhoneNumber != phoneNumber)
            {
                var setPhoneResult = await _userManager.SetPhoneNumberAsync(user, Input.PhoneNumber);
                if (!setPhoneResult.Succeeded)
                {
                    var userId = await _userManager.GetUserIdAsync(user);
                    throw new InvalidOperationException($"Unexpected error occurred setting phone number for user with ID '{userId}'.");
                }
            }

            user.FirstName = Input.FirstName;
            user.LastName = Input.LastName;
            user.UserName = Input.UserName;
          
            if (file != null)
            {
                Users Result = await _userManager.FindByIdAsync(user.Id);

                if (Result != null)
                {
                    string Files = Path.Combine(_hostingEnvironment.WebRootPath, "ProfileImage");// (_hostingEnvironment.WebRootPath + @"/ProfileImage/" + users.ImagePath);
                    if (System.IO.File.Exists(Files + "\\" + user.ImagePath))
                    {
                        System.IO.File.Delete(Files + "\\" + user.ImagePath);
                    }

                }

                string UploadFolder = Path.Combine(_hostingEnvironment.WebRootPath, "ProfileImage");
                if (!Directory.Exists(UploadFolder))
                {
                    Directory.CreateDirectory(UploadFolder);
                }
                var uniqueFileName = Guid.NewGuid().ToString() + "_" + file.FileName;
                string filePath = Path.Combine(UploadFolder, uniqueFileName);
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }
                user.ImagePath = uniqueFileName;

            }

            await _userManager.UpdateAsync(user);

            await _signInManager.RefreshSignInAsync(user);
            TempData["SuccesMessage"] = "Your profile has been updated";

            return RedirectToPage();
        }
        public async Task<IActionResult> OnPostSendVerificationEmailAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {

                TempData["ErrorMessage"] = "Unable to load user with ID";
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }


            var userId = await _userManager.GetUserIdAsync(user);
            var email = await _userManager.GetEmailAsync(user);
            var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            var callbackUrl = Url.Page(
                "/Account/ConfirmEmail",
                pageHandler: null,
                values: new { userId = userId, code = code },
                protocol: Request.Scheme);
            //await _emailSender.SendEmailAsync(
            //    email,
            //    "Confirm your email",
            //    $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

            string bodyTemplate = System.IO.File.ReadAllText(Path.Combine(_hostingEnvironment.WebRootPath, "EmailTemplate/EmailVerificationLink.html"));
            bodyTemplate = bodyTemplate.Replace("[confirmationLink]", HtmlEncoder.Default.Encode(callbackUrl));

            await _emailSender.SendEmailAsyncWithBody(user.Email, "Email Verification", bodyTemplate, true);

            //  await _signInManager.SignInAsync(user, isPersistent: false);
            TempData["SuccesMessage"] = "Verification email sent. Please check your email.";

            //  StatusMessage = "Verification email sent. Please check your email.";
            return RedirectToPage();
        }
    }
}
