﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using ImagingUtilizationUpload.Models;
using Microsoft.AspNetCore.Identity;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting.Internal;
using ImagingUtilizationUpload.Services;
using System.IO;
using ImagingUtilizationUpload.Data;

namespace ImagingUtilizationUpload.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class RegisterModel : PageModel
    {
        private readonly SignInManager<Users> _signInManager;
        private readonly UserManager<Users> _userManager;
        private readonly ILogger<RegisterModel> _logger;
        private readonly IEmailSender _emailSender;
        private IHostingEnvironment _hostingEnvironment;
        private readonly RoleManager<Roles> _roleManager;
        private readonly ApplicationDbContext _context;

        public RegisterModel(
          UserManager<Users> userManager,
          SignInManager<Users> signInManager,
          ILogger<RegisterModel> logger,
          IEmailSender emailSender,
          IHostingEnvironment environment,
           RoleManager<Roles> roleManager,
           ApplicationDbContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _emailSender = emailSender;
            _hostingEnvironment = environment;
            _roleManager = roleManager;
            _context = context;
        }
        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public string ErrorMessage { get; set; }

        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        public class InputModel
        {
            [Required]
            [Display(Name = "Email")]
            [RegularExpression(@"^\s*[\w\-\+_']+(\.[\w\-\+_']+)*\@[A-Za-z0-9]([\w\.-]*[A-Za-z0-9])?\.[A-Za-z][A-Za-z\.]*[A-Za-z]$", ErrorMessage = "Invalid Email Address")]
            public string Email { get; set; }

            [Required]
            [Display(Name = "User Name")]
            public string UserName { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }

            [Required]
            [Display(Name = "Role Name")]
            public string Name { get; set; }

            [TempData]
            public string ErrorMessage { get; set; }
        }

        public async Task OnGetAsync(string returnUrl = null)
        {

            ViewData["roles"] = _roleManager.Roles.Where(m => m.IsDelete == false || m.IsDelete == null).ToList();
            ReturnUrl = returnUrl;
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");
            // search role
            var role = _roleManager.FindByIdAsync(Input.Name).Result;
            ViewData["roles"] = _roleManager.Roles.Where(m => m.IsDelete == false || m.IsDelete == null).ToList();

            if (ModelState.IsValid)
            {
                var user = new Users { UserName = Input.UserName, Email = Input.Email, RoleId = role.Id };
                user.PasswordExpiryDate = DateTime.UtcNow;
                user.IsDeleted = false;

                var results = _context.Users.Where(m=>m.Email == Input.Email).FirstOrDefault();
                if (results != null)
                {
                    ModelState.AddModelError("Input.Email", "The email already exists. Please use a different email.");
                    return Page();
                }

                var result = await _userManager.CreateAsync(user, Input.Password);
                if (result.Succeeded)
                {
                    _logger.LogInformation("User created a new account with password.");

                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    var callbackUrl = Url.Page(
                        "/Account/ConfirmEmail",
                        pageHandler: null,
                        values: new { userId = user.Id, code = code },
                        protocol: Request.Scheme);

                    string bodyTemplate = System.IO.File.ReadAllText(Path.Combine(_hostingEnvironment.WebRootPath, "EmailTemplate/EmailVerificationLink.html"));
                    bodyTemplate = bodyTemplate.Replace("[confirmationLink]", HtmlEncoder.Default.Encode(callbackUrl));

                    await _emailSender.SendEmailAsyncWithBody(user.Email, "Email Verification", bodyTemplate, true);
                    await _userManager.AddToRoleAsync(user, role.Name);
                    //  await _signInManager.SignInAsync(user, isPersistent: false);
                    TempData["SuccesMessage"] = "Your account has been created, please check you email for verification link";
                    return RedirectToPage("./Login");
                }
                string msg = string.Empty;
                foreach (var error in result.Errors)
                {
                    msg = msg + error.Description;
                }
                if (msg != null)
                {
                    ModelState.AddModelError("ErrorMessage", msg);
                }
                ViewData["roles"] = _roleManager.Roles.Where(m => m.IsDelete == false || m.IsDelete == null).ToList();
            }

            // If we got this far, something failed, redisplay form
            return Page();
         }
     }
}
