﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ImagingUtilizationUpload.Models
{
    public class MenuMaster
    {
        public int Id { get; set; }
        public Nullable<int> ParentMenuId { get; set; }
        [Column(TypeName = "nvarchar(50)")]      
        public string Name { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string ImagePath { get; set; }
        public Nullable<int> DisplayOrder { get; set; }
        public Nullable<bool> IsDisplay { get; set; }
        public Nullable<bool> IsParent { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Controller { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Action { get; set; }
    }
}
