﻿using ImagingUtilizationUpload.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImagingUtilizationUpload.Models
{
    public class DbInitializer
    {
        public static async Task IntitializeAsync(ApplicationDbContext context, IServiceProvider serviceProvider)
        {
            var RoleManager = serviceProvider.GetRequiredService<RoleManager<Roles>>();
            string roleName = "Administrator";

            //IdentityResult roleResult;
            var roleExist = await RoleManager.RoleExistsAsync(roleName.Trim());
            Roles role = new Roles();
            if (!roleExist)
            {
                role = new Roles { Name = roleName.Trim(), IsDelete = false };
                var result = await RoleManager.CreateAsync(role);

            }
            var UserManager = serviceProvider.GetRequiredService<UserManager<Users>>();
            string userName = "Administrator";
            string email = "Administrator@yopmail.com";
            string password = "Administrator@123";

            var userResult = UserManager.FindByNameAsync(userName.Trim()).Result;
            var EmailResult = UserManager.FindByEmailAsync(email).Result;


            if (userResult == null)
            {
                string roleId = "";
                var roleid = RoleManager.FindByNameAsync(roleName.Trim()).Result;
                if (roleid.Id != null)
                {
                    roleId = roleid.Id;
                }
                var user = new Users { UserName = userName.Trim(), Email = email.Trim(), RoleId = roleId };
                user.PasswordExpiryDate = DateTime.UtcNow;
                user.IsDeleted = false;
                user.EmailConfirmed = true;
                var createPowerUser = await UserManager.CreateAsync(user, password);
                if (createPowerUser.Succeeded)
                {
                    //  var confirmationToken = UserManager.GenerateEmailConfirmationTokenAsync(user).Result;
                    // var result = UserManager.ConfirmEmailAsync(user, confirmationToken).Result;
                    //here we tie the new user to the role
                    UserManager.AddToRoleAsync(user, roleName).GetAwaiter().GetResult();

                }
            }

            List<MenuMaster> menuMasters = new List<MenuMaster>();
            
            menuMasters.Add(new MenuMaster { ParentMenuId = 0, Name = "User", ImagePath = "fa fa-user", DisplayOrder = null, IsDisplay = true, IsParent = null, Controller = "User", Action = "Index" });
            menuMasters.Add(new MenuMaster { ParentMenuId = 0, Name = "Role", ImagePath = "fa fa-plus-circle", DisplayOrder = null, IsDisplay = true, IsParent = null, Controller = "Role", Action = "Index" });
            
            var MenuList = context.MenuMaster.ToList();

            foreach (var item in menuMasters)
            {
                if (!context.MenuMaster.Any(a => a.Name == item.Name))
                {
                    context.MenuMaster.Add(item);
                    context.SaveChanges();
                }
                }

            if (MenuList.Count() <= 0)
            {
                string roleId1 = "";
                var roleid1 = RoleManager.FindByNameAsync(roleName.Trim()).Result;
                if (roleid1.Id != null)
                {
                    roleId1 = roleid1.Id;
                }
                var RoleRigtsList = context.RoleMenuMap.Where(x => x.RoleId == roleId1).ToList();
                if (RoleRigtsList.Count() >= 0)
                {

                    // var GerRoleList = RoleRigtsList.Where(m => m.RoleId == role.Id).ToList();
                    foreach (var rigths in RoleRigtsList)
                    {

                        rigths.IsView = true;
                        rigths.IsInsert = true;
                        rigths.IsEdit = true;
                        rigths.IsDelete = true;

                        context.RoleMenuMap.Update(rigths);
                        context.SaveChanges();
                    }
                }
            }


        }
    }
}
