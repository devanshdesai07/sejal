﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ImagingUtilizationUpload.Models
{
    public class RoleMenuList
    {
        public int Id { get; set; }
        public string RoleId { get; set; }
        public int MenuId { get; set; }
        public int ParentMenuId { get; set; }
        public int DisplayOrder { get; set; }
        public bool IsParent { get; set; }
        public bool IsDisplay { get; set; }
        public bool IsView { get; set; }
        public bool IsInsert { get; set; }
        public bool IsEdit { get; set; }
        public bool IsDelete { get; set; }
        [Column(TypeName = "varchar(50)")]       
        public string RoleName { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string MenuName { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string Controller { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string Action { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string IconClass { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string RedirectPath { get; set; }
    }
}
