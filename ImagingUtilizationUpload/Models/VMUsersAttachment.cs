﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ImagingUtilizationUpload.Models
{
    public class VMUsersAttachment
    {
        public string UserId { get; set; }

        [Required]
        [Display(Name = "Email")]
        [RegularExpression(@"^\s*[\w\-\+_']+(\.[\w\-\+_']+)*\@[A-Za-z0-9]([\w\.-]*[A-Za-z0-9])?\.[A-Za-z][A-Za-z\.]*[A-Za-z]$", ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }


        [Display(Name = "Phone Number")]
        [RegularExpression(@"^\(\d{3}\)\s{0,1}\d{3}-\d{4}$", ErrorMessage = "Enter a valid contact number")]
        public string PhoneNumber { get; set; }

        [Required]
        [Display(Name = "User Name")]
        public string UserName { get; set; }


        [Display(Name = "Role")]
        public string RoleId { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        public string ImagePath { get; set; }
        [Required]
        [Display(Name = "Role Name")]
        public string Name { get; set; }

        //[Display(Name = "Beginning Balance")]
        //[DisplayFormat(DataFormatString = "{0:0.00}", ApplyFormatInEditMode = true)]
        //public Nullable<decimal> BeginningBalance { get; set; }

        [Display(Name = "Phone Extension")]
        public string PhoneExtension { get; set; }

        public string ErrorMessage { get; set; }
    }
}
