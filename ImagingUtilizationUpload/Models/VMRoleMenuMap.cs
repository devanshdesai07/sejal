﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImagingUtilizationUpload.Models
{
    public class VMRoleMenuMap
    {
        public int Id { get; set; }
        public string RoleId { get; set; }
        public int MenuId { get; set; }
        public bool IsView { get; set; }
        public bool IsInsert { get; set; }
        public bool IsEdit { get; set; }
        public bool IsDelete { get; set; }
        public string MenuName { get; set; }
        public int ParentMenuId { get; set; }
        public int DisplayOrder { get; set; }
    }
}
