﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ImagingUtilizationUpload.Models
{
    public class Users : IdentityUser
    {
        public Nullable<bool> IsDeleted { get; set; }       
        public Nullable<DateTime> PasswordExpiryDate { get; set; }
        public string RoleId { get; set; }
        [Column(TypeName = "varchar(30)")]
        [StringLength(30, ErrorMessage = "Name cannot be longer than 30 characters.")]
        public string FirstName { get; set; }
        [Column(TypeName = "varchar(30)")]
        [StringLength(30, ErrorMessage = "Name cannot be longer than 30 characters.")]
        public string LastName { get; set; }

        public string ImagePath { get; set; }

    }
}
