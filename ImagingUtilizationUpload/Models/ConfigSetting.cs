﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ImagingUtilizationUpload.Models
{
    public class ConfigSetting
    {
        public int passwordExpiryDay { get; set; }
        [Column(TypeName = "decimal(12,2)")]
        public decimal OverheadRate { get; set; }
        [Column(TypeName = "decimal(12,2)")]
        public decimal straighttimerate { get; set; }
        [Column(TypeName = "decimal(12,2)")]
        public decimal OverTimeRates { get; set; }
        [Column(TypeName = "decimal(12,2)")]
        public decimal DoubleTimeRates { get; set; }
        [Column(TypeName = "decimal(12,2)")]
        public decimal ForemanOverheadRate { get; set; }
        [Column(TypeName = "decimal(12,2)")]
        public decimal ForemanStraightTimerate { get; set; }
        [Column(TypeName = "decimal(12,2)")]
        public decimal ForemanOverTimeRates { get; set; }
        [Column(TypeName = "decimal(12,2)")]
        public decimal ForemanDoubleTimeRates { get; set; }
    }
}
