﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ImagingUtilizationUpload.Models
{
    public class LanguageMaster
    {
        [Key]
        public int LanguageId { get; set; }

        [Required]
        [Column(TypeName = "nvarchar(50)")]
        public string LanguageName { get; set; }
        public bool IsActive { get; set; }
    }

    public class LanguageMasterdetail
    {
        public List<LanguageMaster> LanguageMasterDetail { get; set; }
        public int LanguageId { get; set; }

    }
}
