﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ImagingUtilizationUpload.Models
{
    
    public class Attributes
    {
        public string type { get; set; }
        public string url { get; set; }
    }

    public class BillingAddress
    {
        public string city { get; set; }
        public string country { get; set; }
        public object geocodeAccuracy { get; set; }
        public object latitude { get; set; }
        public object longitude { get; set; }
        public string postalCode { get; set; }
        public string state { get; set; }
        public string street { get; set; }
    }

    public class Record
    {
        public Attributes attributes { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public BillingAddress BillingAddress { get; set; }
        public BillingAddress ShippingAddress { get; set; }
        public string Type__c { get; set; }
        public string Account__c { get; set; }
    }

    public class SalesForceAccount
    {
        public int totalSize { get; set; }
        public bool done { get; set; }
        public List<Record> records { get; set; }

        public int id { get; set; }
        public bool success { get; set; }
        public string errors { get; set; }
        public bool hasErrors { get; set; }
    }
}
