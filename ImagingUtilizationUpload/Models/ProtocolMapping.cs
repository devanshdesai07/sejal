﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ImagingUtilizationUpload.Models
{
    public class ProtocolMapping
    {
        [Key]
        public int ProtocolMappingId { get; set; }
        [Required]
        [Column(TypeName = "nvarchar(50)")]
        public string ProtocolName { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string MapedProtocol { get; set; }
        public bool IsActive { get; set; }
        public int LanguageId { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
    public class ProtocolMappingDetails
    {
        public List<ProtocolMapping> protocolmapping { get; set; }
        public int LanguageId { get; set; }

    }
}
