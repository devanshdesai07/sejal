﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ImagingUtilizationUpload.Models
{
    public class DeviceType
    {
        [Key]
        public int DeviceTypeId { get; set; }
        public string DeviceName { get; set; }
    }
}
