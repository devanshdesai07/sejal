﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImagingUtilizationUpload.Data.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Linq;
    using System.Linq.Expressions;

    /// <summary>
    /// I Repository
    /// </summary>
    /// <typeparam name="T">Interface Repository</typeparam>
    public interface IRepository<T>
    {

        IList<TEntity> ExecuteStoredProcedureList<TEntity>(string commandText, params object[] parameters) where TEntity : class;

        IList<T> ExecuteStoredProcedure<T>(string storedProcName, params object[] parameters);

    }
}
