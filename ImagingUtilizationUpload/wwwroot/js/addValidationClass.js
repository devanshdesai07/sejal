﻿(function () {

    var originalAddClassMethod = jQuery.fn.addClass;

    jQuery.fn.addClass = function () {
        // Execute the original method.
        var result = originalAddClassMethod.apply(this, arguments);

        // trigger a custom event
        // stop recursion...
        if ((arguments[0] !== "is-valid") && (arguments[0] !== "is-invalid")) {
            jQuery(this).trigger('cssClassChanged');
        }

        // return the original result
        return result;
    }
})();

// document ready function
$(function () {
    $("input,select,textarea").bind('cssClassChanged', function () {
        // cleanup
        if ($(this).hasClass("is-valid")) {
            $(this).removeClass("is-valid");
        }
        if ($(this).hasClass("is-invalid")) {
            $(this).removeClass("is-invalid");
        }

        // remap the css classes to that of BootStrap 
        if ($(this).hasClass("input-validation-error")) {
            $(this).addClass("is-invalid");
            $(this).next().addClass("invalid-feedback");
        }

        if ($(this).hasClass("valid")) {
            //$(this).addClass("is-valid");
            $(this).next().removeClass("invalid-feedback");
        }
    });
});