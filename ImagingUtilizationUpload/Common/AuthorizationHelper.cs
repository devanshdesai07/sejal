﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImagingUtilizationUpload.Common
{
    public class AuthorizationHelper
    {
        public static bool CanAdd(string controllerName)
        {
            return SessionHelper.RoleMenuList.Any(item => item.IsInsert == true && item.Controller != null && item.Controller.ToLower() == controllerName.ToLower());
        }

        public static bool CanEdit(string controllerName)
        {
            return SessionHelper.RoleMenuList.Any(item => item.IsEdit == true && item.Controller != null && item.Controller.ToLower() == controllerName.ToLower());
        }

        public static bool CanDelete(string controllerName)
        {
            return SessionHelper.RoleMenuList.Any(item => item.IsDelete == true && item.Controller != null && item.Controller.ToLower() == controllerName.ToLower());
        }
        public static bool CanEditDelete(string controllerName)
        {
            return SessionHelper.RoleMenuList.Any(item => (item.IsDelete == true || item.IsEdit == true)

            && item.Controller != null && item.Controller.ToLower() == controllerName.ToLower());
        }
        public static bool CanEditDeleteInsert(string controllerName)
        {
            return SessionHelper.RoleMenuList.Any(item => (item.IsDelete == true || item.IsEdit == true || item.IsInsert == true)

            && item.Controller != null && item.Controller.ToLower() == controllerName.ToLower());
        }
        public static bool IsAuthorized(string controllerName)
        {
            return SessionHelper.RoleMenuList.Any(item => item.IsView == true && item.Controller != null && item.Controller.ToLower() == controllerName.ToLower());
        }
    }
}
