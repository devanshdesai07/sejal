﻿using ImagingUtilizationUpload.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImagingUtilizationUpload.Common
{
    public class SessionHelper
    {
        public static string UserId
        {
            get => HttpHelper.HttpContext.Session.GetString("UserId") == null ? "" : HttpHelper.HttpContext.Session.GetString("UserId");
            set => HttpHelper.HttpContext.Session.SetString("UserId", value);
        }       
        public static List<RoleMenuList> RoleMenuList
        {
            get
            {
                if (HttpHelper.HttpContext.Session.GetString("RoleMenuList") == null)
                {
                    return null;
                }
                else
                {
                    return JsonConvert.DeserializeObject<List<RoleMenuList>>(Convert.ToString(HttpHelper.HttpContext.Session.GetString("RoleMenuList")));
                }
            }

            set
            {
                HttpHelper.HttpContext.Session.SetString("RoleMenuList", JsonConvert.SerializeObject(value));//= JsonConvert.SerializeObject(value);
            }
        }
    }
}
