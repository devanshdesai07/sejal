﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ImagingUtilizationUpload.Common
{
    public class HttpHelper
    {
        private static IHttpContextAccessor HttpContextAccessor;

        public static void Configure(IHttpContextAccessor httpContextAccessor)
        {
            HttpContextAccessor = httpContextAccessor;
        }

        public static HttpContext HttpContext => HttpContextAccessor.HttpContext;

        public static void GenerateLogfile(string ControllerName, string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\ImagingUtilizationUploadLog";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\ImagingUtilizationUploadLog\\ImagingUtilizationUploadLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_');

            if (!System.IO.File.Exists(filepath))
            {
                using (StreamWriter sw = System.IO.File.CreateText(filepath))
                {
                    sw.WriteLine("Master Page: " + ControllerName);
                    sw.WriteLine("Exception Message: " + Message);
                }
            }
            else
            {
                System.IO.File.Delete(filepath);

                using (StreamWriter sw = System.IO.File.CreateText(filepath))
                {
                    sw.WriteLine("Master Page: " + ControllerName);
                    sw.WriteLine("Exception Message: " + Message);
                }
            }
        }
    }
}
