﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace ImagingUtilizationUpload.Common
{
    public class SalesforceClient
    {
        private readonly IConfiguration _configuration;
        public string Username { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string AuthToken { get; set; }
        public string InstanceUrl { get; set; }
        public string grant_type { get; set; }
        public string getAccountDetails_Endpoint { get; set; }
        public string GetAddressDetails_Endpoint { get; set; }
        public string UploadDataToSalesforce_Endpoint { get; set; }
        public SalesforceClient(IConfiguration configuration)
        {
            _configuration = configuration;
            // SF requires TLS 1.1 or 1.2
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
            this.Username = _configuration.GetSection("Salesforce:username").Value;
            this.Password = _configuration.GetSection("Salesforce:password").Value;
            this.ClientId = _configuration.GetSection("Salesforce:client_id").Value;
            this.ClientSecret = _configuration.GetSection("Salesforce:client_secret").Value;
            this.InstanceUrl = _configuration.GetSection("Salesforce:Login_Endpoint").Value;
            this.grant_type = _configuration.GetSection("Salesforce:grant_type").Value;
            this.getAccountDetails_Endpoint = _configuration.GetSection("APIUrls:getAccountDetails_Endpoint").Value;
            this.GetAddressDetails_Endpoint = _configuration.GetSection("APIUrls:GetAddressDetails_Endpoint").Value;
            this.UploadDataToSalesforce_Endpoint = _configuration.GetSection("APIUrls:UploadDataToSalesforce_Endpoint").Value;
            var Client = new HttpClient();
        }

        public string generateSalesForceToken()
        {
            var client = new RestClient(InstanceUrl);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Cookie", "BrowserId=JxJP-1BQEeyo6G-MHYWgKA; CookieConsentPolicy=0:0; LSKey-c$CookieConsentPolicy=0:0");
            request.AlwaysMultipartFormData = true;
            request.AddParameter("username", Encoding.UTF8.GetString(Convert.FromBase64String(Username)));
            request.AddParameter("password", Encoding.UTF8.GetString(Convert.FromBase64String(Password)));
            request.AddParameter("grant_type", grant_type);
            request.AddParameter("client_id", Encoding.UTF8.GetString(Convert.FromBase64String(ClientId)));
            request.AddParameter("client_secret", Encoding.UTF8.GetString(Convert.FromBase64String(ClientSecret)));
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.Content);
            return response.Content;
        }

        public string getAccountDetails(string token,string SerialNumber)
        {
            var client = new RestClient(getAccountDetails_Endpoint + "'" + SerialNumber + "'");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + token);
            request.AddHeader("Cookie", "BrowserId=JxJP-1BQEeyo6G-MHYWgKA; CookieConsentPolicy=0:1; LSKey-c$CookieConsentPolicy=0:1");
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string GetAddressDetails(string token,string Account__c)
        {
            var client = new RestClient(GetAddressDetails_Endpoint + "'" + Account__c + "'");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + token);
            request.AddHeader("Cookie", "BrowserId=JxJP-1BQEeyo6G-MHYWgKA; CookieConsentPolicy=0:1; LSKey-c$CookieConsentPolicy=0:1");
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public async Task<string> UploadDataToSalesforce(string token, string rawJson)
        {
            //var client = new RestClient(UploadDataToSalesforce_Endpoint);
            //client.Timeout = -1;
            //var request = new RestRequest(Method.POST);
            //request.AddHeader("Accept", "application/json");
            //request.Parameters.Clear();
            //request.AddHeader("Authorization", "Bearer " + token);
            //request.AddParameter("application/json", rawJson, ParameterType.RequestBody);
            //request.AddHeader("Cookie", "BrowserId=JxJP-1BQEeyo6G-MHYWgKA; CookieConsentPolicy=0:1; LSKey-c$CookieConsentPolicy=0:1");
            //IRestResponse response = client.Execute(request);
            //return response.Content;

            //var client = new RestClient(UploadDataToSalesforce_Endpoint);
            //client.Timeout = -1;
            //var request = new RestRequest(Method.GET);
            //request.AddHeader("Authorization", "Bearer " + token);
            //request.AddHeader("Content-Type", "application/json");
            //request.AddHeader("Cookie", "BrowserId=I6wQaFmZEeyQXr2c3oKq4A; CookieConsentPolicy=0:1; LSKey-c$CookieConsentPolicy=0:1");
            //var body = JsonConvert.DeserializeObject(rawJson);
            //request.AddParameter("application/json", body, ParameterType.RequestBody);
            //IRestResponse response = client.Execute(request);
            ////Console.WriteLine(response.Content);
            //return response.Content;

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var request = new HttpRequestMessage
              {
                  Method = HttpMethod.Post,
                  RequestUri = new Uri(UploadDataToSalesforce_Endpoint),
                  Content = new StringContent(rawJson, Encoding.UTF8, MediaTypeNames.Application.Json)
              };

            var response = await client.SendAsync(request).ConfigureAwait(false);
      //      response.EnsureSuccessStatusCode();

            var responseBody = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            return responseBody;
        }

    }
}

