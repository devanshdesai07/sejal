using ImagingUtilizationUpload.Data;
using ImagingUtilizationUpload.Models;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImagingUtilizationUpload
{
    public class Program
    {
        //public static void Main(string[] args)
        //{
        //    //CreateHostBuilder(args).Build().Run();
        //}
        public static void Main(string[] args)
        {
          //  CreateHostBuilder(args).Build().Run();
            var host = BuildWebHost(args);
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                {
                    var context = services.GetRequiredService<ApplicationDbContext>();
                    DbInitializer.IntitializeAsync(context, services).Wait();
                }
                catch (Exception ex)
                {
                    var Logger = services.GetRequiredService<ILogger<Program>>();
                    Logger.LogError(ex, "An error occurred while seeding the database.");
                    throw ex;
                }
            }
            host.Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
                   WebHost.CreateDefaultBuilder(args)
                   .UseStartup<Startup>()
                   .Build();

        //public static IHostBuilder CreateHostBuilder(string[] args) =>
        //  Host.CreateDefaultBuilder(args)
        //   .ConfigureAppConfiguration((context, config) =>
        //   {

        //       var root = config.Build();
        //       config.AddAzureKeyVault($"https://{root["KeyVault:Vault"]}.vault.azure.net/", root["KeyVault:ClientId"], root["KeyVault:ClientSecret"]);
        //   })
        //   .ConfigureWebHostDefaults(webBuilder =>
        //   {
        //       webBuilder.UseStartup<Startup>();
        //   });
    }
}
